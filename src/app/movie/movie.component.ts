import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  @Input() data:any; //מהבן לאבא-לא לשכוח לעשות באנגי און איניט את כל התכונות 
  @Output() delete = new EventEmitter<any>();  //אבא לבן

  id;
  title;
  studio;
  weekendIncome;
  
  constructor() { }

  ngOnInit() {
    this.id= this.data.id;
    this.title= this.data.title;
    this.studio= this.data.studio;
    this.weekendIncome= this.data.weekendIncome;

    
  }
  deleteMovie() {//
    
    this.delete.emit(this);//לוקחת את המשתנה אוטפוט דיליט שמעביר את כל הסרט לאבא
  }

}
